package com.bncc.RoomManager.enums;

public enum ReportStatusEnum {
    ACTIVE,
    SOLVED,
    CLOSED,
}
