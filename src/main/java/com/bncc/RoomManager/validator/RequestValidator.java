package com.bncc.RoomManager.validator;

import com.bncc.RoomManager.dto.request.BaseRequest;
import com.bncc.RoomManager.dto.request.report.FetchReportRequest;
import com.bncc.RoomManager.dto.request.report.ReportIssueRequest;
import com.bncc.RoomManager.validator.command.ReportIssueRequestValidator;

import java.util.HashMap;
import java.util.Map;

public class RequestValidator {
//    public static Map<Class<? extends BaseRequest>, BaseValidator> validatorMap = new HashMap<>();
//    static {
//        validatorMap.put(ReportIssueRequest.class, new ReportIssueRequestValidator());
//        validatorMap.put(FetchReportRequest.class, new ReportIssueRequestValidator());
//    }
//
//    public  static  Map<String, String> validate(BaseRequest baseRequest){
//        BaseValidator baseValidator = validatorMap.get(baseRequest.getClass());
//        return baseValidator.validate(baseRequest);
//    }
    public Map<String, String> validate(BaseValidator baseValidator, BaseRequest baseRequest){
        return baseValidator.validate(baseRequest);
    }

}
