package com.bncc.RoomManager.validator.command;

import com.bncc.RoomManager.dto.request.BaseRequest;
import com.bncc.RoomManager.dto.request.report.ReportIssueRequest;
import com.bncc.RoomManager.validator.BaseValidator;

import java.util.HashMap;
import java.util.Map;

public class ReportIssueRequestValidator  implements BaseValidator {
    @Override
    public Map<String, String> validate(BaseRequest baseRequest) {

        Map<String, String> errorList =new HashMap<>();

        if(!(baseRequest instanceof  ReportIssueRequest)){
            errorList.put("Object Error", "Not Type of Report Issue");
            return errorList;
        }

        ReportIssueRequest reportIssueRequest = (ReportIssueRequest) baseRequest;

        if(reportIssueRequest.getRoomId() ==0){
            errorList.put("roomId", "Room can't be null");
        }
        if (reportIssueRequest.getDescription().length()==0){
            errorList.put("description","description can't be null");
        }
        if(reportIssueRequest.getTitle().length()<10){
            errorList.put ("title", "Title can't be less than 10");
        }
        if(errorList.size()>0) return errorList;

        return null;
    }
}
