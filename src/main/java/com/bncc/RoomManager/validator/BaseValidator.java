package com.bncc.RoomManager.validator;

import com.bncc.RoomManager.dto.request.BaseRequest;

import java.util.Map;

public interface BaseValidator {

    public Map<String, String> validate(BaseRequest baseRequest);
}
