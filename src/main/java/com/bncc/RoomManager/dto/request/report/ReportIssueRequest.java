package com.bncc.RoomManager.dto.request.report;

import com.bncc.RoomManager.dto.request.BaseRequest;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ReportIssueRequest extends BaseRequest {
    private String title;
    private String description;
    private long roomId;
}
