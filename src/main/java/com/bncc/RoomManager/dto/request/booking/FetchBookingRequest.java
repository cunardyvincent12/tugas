package com.bncc.RoomManager.dto.request.booking;

import com.bncc.RoomManager.dto.request.BaseRequest;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@NoArgsConstructor
@Accessors(chain=true)
public class FetchBookingRequest extends BaseRequest {
    private String title;
    private Date dateStart;
    private Date dateEnd;
    private  long roomId;
}
