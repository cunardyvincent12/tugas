package com.bncc.RoomManager.dto.request.report;

import com.bncc.RoomManager.dto.request.BaseRequest;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.domain.Pageable;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class FetchReportRequest extends BaseRequest {

    private Pageable pageable;
}
