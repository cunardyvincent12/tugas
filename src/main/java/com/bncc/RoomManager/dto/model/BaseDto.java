package com.bncc.RoomManager.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BaseDto {

    @JsonIgnore
    BaseError error;
}
