package com.bncc.RoomManager.dto.model.room;

import com.bncc.RoomManager.dto.model.BaseDto;

public class RoomCreateDto extends BaseDto {

    private long id;
    private String roomName;
    private int capacity;
    private String description;

    public RoomCreateDto(long id, String roomName, int capacity, String description) {
        this.id = id;
        this.roomName = roomName;
        this.capacity = capacity;
        this.description = description;
    }
    public RoomCreateDto(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
