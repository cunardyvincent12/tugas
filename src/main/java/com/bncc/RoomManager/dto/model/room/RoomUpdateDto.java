package com.bncc.RoomManager.dto.model.room;

public class RoomUpdateDto {
    private long id;
    private String roomName;
    private int capacity;
    private String description;

    public RoomUpdateDto(long id, String roomName, int capacity, String description) {
        this.id = id;
        this.roomName = roomName;
        this.capacity = capacity;
        this.description = description;
    }
    public RoomUpdateDto(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
