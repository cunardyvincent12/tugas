package com.bncc.RoomManager.dto.model.report;

import com.bncc.RoomManager.enums.ReportStatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain =true)
public class ReportDto {

    private  long id;
    private String title;
    private String description;
    private ReportStatusEnum status;

    private long roomId;

}

