package com.bncc.RoomManager.dto.model.booking;

import com.bncc.RoomManager.enums.BookingStatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@NoArgsConstructor
@Accessors(chain =true)
public class BookingDto {
    private long id;
    private String title;
    private Date dateStart;
    private Date dateEnd;
    private BookingStatusEnum status;
    private  long roomId;
}
