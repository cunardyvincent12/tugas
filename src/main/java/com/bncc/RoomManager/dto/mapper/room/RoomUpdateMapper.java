package com.bncc.RoomManager.dto.mapper.room;

import com.bncc.RoomManager.domain.Room;
import com.bncc.RoomManager.dto.model.room.RoomUpdateDto;

public class RoomUpdateMapper {
    public static RoomUpdateDto convertEntityToDto(Room room){
        return new RoomUpdateDto(
                room.getId(),
                room.getRoomName(),
                room.getCapacity(),
                room.getDescription()
        );
    }
}
