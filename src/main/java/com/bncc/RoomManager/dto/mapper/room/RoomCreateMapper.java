package com.bncc.RoomManager.dto.mapper.room;

import com.bncc.RoomManager.domain.Room;
import com.bncc.RoomManager.dto.model.room.RoomCreateDto;

public class RoomCreateMapper {
    public static RoomCreateDto convertEntityToDto(Room room) {
        return new RoomCreateDto(
                room.getId(),
                room.getRoomName(),
                room.getCapacity(),
                room.getDescription()
        );
    }
}
