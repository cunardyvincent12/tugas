package com.bncc.RoomManager.dto.mapper.booking;

import com.bncc.RoomManager.domain.Booking;
import com.bncc.RoomManager.dto.model.booking.BookingDto;

public class BookingMapper {
    public static BookingDto convertToDto(Booking booking){
        return new BookingDto().setTitle(booking.getTitle())
                .setDateStart(booking.getDateStart())
                .setDateEnd(booking.getDateEnd())
//                .setStatus(booking.getStatus())
                .setId(booking.getId())
                ;
    }
}
