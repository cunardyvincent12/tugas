package com.bncc.RoomManager.dto.mapper.report;

import com.bncc.RoomManager.domain.Report;
import com.bncc.RoomManager.dto.model.report.ReportDto;

public class ReportMapper {

    public static ReportDto convertToDto(Report report){
        return new ReportDto().setDescription(report.getDescription())
                .setId(report.getId()).setRoomId(report.getRoom().getId())
                .setStatus(report.getStatus()).setTitle((report.getTitle()));
    }
}
