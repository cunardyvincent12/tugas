package com.bncc.RoomManager.controller;

import com.bncc.RoomManager.domain.Room;
import com.bncc.RoomManager.dto.model.room.RoomCreateDto;
import com.bncc.RoomManager.dto.model.room.RoomUpdateDto;
import com.bncc.RoomManager.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RoomController {

    @Autowired
    RoomService roomService;
    @RequestMapping(method = RequestMethod.GET,value ="/vl")
    public List<Room> fetchAllRoom(){
       return roomService.fetchAllRoom();
    }



    @RequestMapping(method = RequestMethod.POST, value="/post")
    public RoomCreateDto tambahRuangan(@RequestBody Room room){
       return roomService.createRoom(room);

    }

    @RequestMapping(method = RequestMethod.PUT, value="/update")
    public RoomUpdateDto updateRoom(@RequestBody Room room){
      return  roomService.updateRoom(room);

    }
}
