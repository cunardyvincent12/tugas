package com.bncc.RoomManager.controller;

import com.bncc.RoomManager.domain.Report;
import com.bncc.RoomManager.dto.model.report.ReportDto;
import com.bncc.RoomManager.dto.request.report.FetchReportRequest;
import com.bncc.RoomManager.dto.request.report.ReportIssueRequest;
import com.bncc.RoomManager.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    ReportService reportService;

    @RequestMapping(method = RequestMethod.GET, value="/view")
    public ResponseEntity fetchAllReport(@PageableDefault Pageable pageable){
        FetchReportRequest fetchReportRequest = new FetchReportRequest().setPageable(pageable);

        Page<Report> reportPage = reportService.findAllReport(fetchReportRequest);
        return  ResponseEntity.ok().body(reportPage);
    }
    @RequestMapping(method = RequestMethod.POST, value="/post")
    public ResponseEntity<ReportDto> postNewIssues(@RequestBody ReportIssueRequest reportIssueRequest){
        System.out.println(reportIssueRequest.toString());
        ReportDto report = reportService.reportNewIssue(reportIssueRequest);
        return  ResponseEntity.ok().body(report);
    }

}
