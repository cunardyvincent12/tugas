package com.bncc.RoomManager.domain;

import com.bncc.RoomManager.domain.base.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Room extends BaseEntity {


    @Column(unique = true)
    private String roomName;

    private int capacity;

    private String description;




}
