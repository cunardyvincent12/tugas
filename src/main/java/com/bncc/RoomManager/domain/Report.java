package com.bncc.RoomManager.domain;

import com.bncc.RoomManager.domain.base.BaseEntity;
import com.bncc.RoomManager.enums.ReportStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Report extends BaseEntity {

    private String title;
    private String description;
    private ReportStatusEnum status;


    @ManyToOne
    private Room room;
}
