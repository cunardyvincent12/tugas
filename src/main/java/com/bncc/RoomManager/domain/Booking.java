package com.bncc.RoomManager.domain;

import com.bncc.RoomManager.domain.base.BaseEntity;
import com.bncc.RoomManager.enums.ReportStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Booking extends BaseEntity {

    private String title;
    private Date dateStart;
    private Date dateEnd;
    private ReportStatusEnum status;

    @JsonIgnore
    @ManyToOne
    private Room room;
}
