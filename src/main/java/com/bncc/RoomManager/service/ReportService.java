package com.bncc.RoomManager.service;

import com.bncc.RoomManager.domain.Report;
import com.bncc.RoomManager.dto.model.report.ReportDto;
import com.bncc.RoomManager.dto.request.report.FetchReportRequest;
import com.bncc.RoomManager.dto.request.report.ReportIssueRequest;
import org.springframework.data.domain.Page;

public interface ReportService {

    Page<Report> findAllReport(FetchReportRequest fetchReportRequest);

    ReportDto reportNewIssue(ReportIssueRequest reportIssueRequest);
}
