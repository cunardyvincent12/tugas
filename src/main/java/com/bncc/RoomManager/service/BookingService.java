package com.bncc.RoomManager.service;

import com.bncc.RoomManager.domain.Booking;
import com.bncc.RoomManager.dto.model.booking.BookingDto;

import java.util.List;

public interface BookingService {

    public List<Booking> fetchAllBooking();
    public BookingDto createBooking();
    public void updateBooking();
    public void cancelBooking();
}
