package com.bncc.RoomManager.service;

import com.bncc.RoomManager.domain.Room;
import com.bncc.RoomManager.dto.model.room.RoomCreateDto;
import com.bncc.RoomManager.dto.model.room.RoomUpdateDto;

import java.util.List;

public interface RoomService {

    public List<Room> fetchAllRoom();
    public RoomCreateDto createRoom(Room room);
    public RoomUpdateDto updateRoom(Room room);
    public void deleteRoom(Room room);

}
