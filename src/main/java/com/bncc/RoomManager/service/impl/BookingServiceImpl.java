package com.bncc.RoomManager.service.impl;

import com.bncc.RoomManager.domain.Booking;
import com.bncc.RoomManager.dto.mapper.booking.BookingMapper;
import com.bncc.RoomManager.dto.model.booking.BookingDto;
import com.bncc.RoomManager.repository.BookingRepository;
import com.bncc.RoomManager.repository.RoomRepository;
import com.bncc.RoomManager.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    RoomRepository roomRepository;

    @Override
    public List<Booking> fetchAllBooking() {
        return bookingRepository.findAll();
    }

    @Override
    public BookingDto createBooking(Booking booking) {
        bookingRepository.save(booking);
        return BookingMapper.convertToDto(booking);

    }

    @Override
    public void updateBooking() {

    }

    @Override
    public void cancelBooking() {

    }
}
