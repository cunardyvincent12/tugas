package com.bncc.RoomManager.service.impl;

import com.bncc.RoomManager.domain.Report;
import com.bncc.RoomManager.domain.Room;
import com.bncc.RoomManager.dto.mapper.report.ReportMapper;
import com.bncc.RoomManager.dto.model.report.ReportDto;
import com.bncc.RoomManager.dto.request.report.FetchReportRequest;
import com.bncc.RoomManager.dto.request.report.ReportIssueRequest;
import com.bncc.RoomManager.enums.ReportStatusEnum;
import com.bncc.RoomManager.repository.ReportRepository;
import com.bncc.RoomManager.repository.RoomRepository;
import com.bncc.RoomManager.service.ReportService;
import com.bncc.RoomManager.validator.RequestValidator;
import com.bncc.RoomManager.validator.command.FetchReportRequestValidator;
import com.bncc.RoomManager.validator.command.ReportIssueRequestValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class ReportServiceImpl implements ReportService {

   private static Logger logger = LoggerFactory.getLogger(ReportService.class);

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    RequestValidator requestValidator;

    @Override
    public Page <Report> findAllReport(FetchReportRequest fetchReportRequest) {
        Map<String, String> validatorResult = requestValidator.validate(new FetchReportRequestValidator(), fetchReportRequest);

        if (validatorResult!=null){
            logger.error("Validation Error", validatorResult.toString());
            return null;
        }
        Page<Report> reports = reportRepository.findAll(fetchReportRequest.getPageable());
        return reports;
    }

    @Override
    public ReportDto reportNewIssue(ReportIssueRequest reportIssueRequest) {
        Map<String,String> validatorResult = requestValidator.validate(new ReportIssueRequestValidator(),reportIssueRequest);

        if( validatorResult ==null){
            logger.error("Service Error", validatorResult.toString());
            return null;
        }
        Room room = roomRepository.findById(reportIssueRequest.getRoomId()).orElse(null);

        if (room == null){
            logger.error("User Not Found");
            return null;
        }
        Report report = new Report(). setTitle(reportIssueRequest.getTitle())
                .setDescription(reportIssueRequest.getDescription())
                .setStatus(ReportStatusEnum.ACTIVE)
                .setRoom(room);
        reportRepository.save(report);

        logger.info("Report Service id"+ report.getId()+"Success");

        return ReportMapper.convertToDto(report);

    }
}
