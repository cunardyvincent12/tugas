package com.bncc.RoomManager.service.impl;

import com.bncc.RoomManager.domain.Room;
import com.bncc.RoomManager.dto.mapper.room.RoomCreateMapper;
import com.bncc.RoomManager.dto.mapper.room.RoomUpdateMapper;
import com.bncc.RoomManager.dto.model.room.RoomCreateDto;
import com.bncc.RoomManager.dto.model.room.RoomUpdateDto;
import com.bncc.RoomManager.repository.RoomRepository;
import com.bncc.RoomManager.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    RoomRepository roomRepository;

    @Override
    public List<Room> fetchAllRoom() {
        return roomRepository.findAll();
    }

    @Override
    public RoomCreateDto createRoom(Room room) {
        roomRepository.save(room);
        return RoomCreateMapper.convertEntityToDto(room);
    }

    @Override
    public RoomUpdateDto updateRoom(Room room) {
         Room tempRoom =  roomRepository.findById(room.getId()).orElse(null);
         if (tempRoom == null){
             return null;
         }

         tempRoom.setRoomName(room.getRoomName());
         tempRoom.setCapacity(room.getCapacity());
         tempRoom.setDescription(room.getDescription());

         roomRepository.save(tempRoom);
         return RoomUpdateMapper.convertEntityToDto(room);
    }

    @Override
    public void deleteRoom(Room room) {

    }
}

