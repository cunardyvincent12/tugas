package com.bncc.RoomManager.repository;

import com.bncc.RoomManager.domain.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    @Override
    List<Room> findAll();

    List<Room> findByRoomNameLike(String name);

    @Override
    Optional<Room> findById(Long id);
}
