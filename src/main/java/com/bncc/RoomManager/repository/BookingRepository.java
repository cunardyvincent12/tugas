package com.bncc.RoomManager.repository;

import com.bncc.RoomManager.domain.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    @Override
    List<Booking> findAll();

    @Override
    Optional<Booking> findById(Long id);
}
