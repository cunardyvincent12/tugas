package com.bncc.RoomManager.repository;

import com.bncc.RoomManager.domain.Report;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

    @Override
    Page<Report> findAll(Pageable pageable);
}
